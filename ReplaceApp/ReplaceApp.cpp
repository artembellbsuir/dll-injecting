﻿#include <iostream>
#include <fstream>
#include <string>
#include "windows.h"

#define LIBRARY_NAME "MyLib.dll"

typedef int(__cdecl* DLL_PROC_SUM)(LPSTR, LPSTR);

int main()
{
	std::cout << "Injecting DLL dynamically." << std::endl;

	DLL_PROC_SUM replaceProc;
	HINSTANCE hLibrary;

	BOOL as;
	//hLibrary = LoadLibraryA(LIBRARY_NAME);

	if (/*NULL != hLibrary*/TRUE) {
		//replaceProc = (DLL_PROC_SUM)::GetProcAddress(hLibrary, "replace");

		if (/*NULL != replaceProc*/TRUE) {
			::Sleep(2000);
			std::ifstream filePid("../ZombieApp/processId.txt");
			std::string processIdStr;
			int processId;

			filePid >> processIdStr;
			filePid.close();

			processId = std::stoi(processIdStr);
			std::cout << processId << std::endl;


			// retreie handle to the remote process
			HANDLE hProcess = ::OpenProcess(
				//PROCESS_VM_READ | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_CREATE_THREAD | PROCESS_CREATE_PROCESS,
				PROCESS_ALL_ACCESS,
				//PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
				FALSE,
				processId
			);
			BOOL openProcessResult = NULL != hProcess;

			if (openProcessResult) {
				auto dllName = "D:/bsuir/5sem/osisp/Libraries/Debug/MyLib.dll";


				// allocate mem for DLL name in the remote process
				LPVOID allocatedRegionAddr = ::VirtualAllocEx(
					hProcess,
					NULL,				// pointer to desired allocated memory address (NULL - function determines where to allocate the region)
					strlen(dllName),	// size of region in memory to allocate (in bytes)
					MEM_COMMIT,			// type of memory allocation (???)
					PAGE_EXECUTE_READWRITE 
					/* memory protection for the region of pages to be allocated
					(for commited pages - read-only or read/write access to the committed region of pages) */
				);
				BOOL virtualAllocResult = NULL != allocatedRegionAddr;


				if (virtualAllocResult) {
					// write the DLL name, including full path, to the allocated memory 
					// hProcess must have PROCESS_VM_WRITE and PROCESS_VM_OPERATION 
					BOOL writeProcessMemResult = ::WriteProcessMemory(
						hProcess,
						allocatedRegionAddr,		// base address of the allocated region of pages
						dllName,					// pointer to the buffer that contains data to be written in the address space of the specified process
						strlen(dllName),			// number of bytes to be written to the specified process
						NULL						// pointer to a variable that receives the number of bytes transferred into the specified process (NULL - ignored)
					);


					if (writeProcessMemResult) {
						HMODULE hKernel32 = ::GetModuleHandleA("Kernel32");
						LPVOID loadLibAddr = (LPVOID)::GetProcAddress(hKernel32, "LoadLibraryA");


						// creates a thread that runs in the virtual address space of another process
						// hProcess must have PROCESS_CREATE_THREAD, PROCESS_QUERY_INFORMATION, PROCESS_VM_OPERATION, PROCESS_VM_WRITE, and PROCESS_VM_READ
						HANDLE hRemoteThread = ::CreateRemoteThread(
							hProcess,
							NULL,					// pointer to a SECURITY_ATTRIBUTES structure that specifies a security descriptor (NULL - thread gets default security descriptor)
							0,						// initial size of the stack, in bytes (0 - new thread uses the default size for the executable)
							(LPTHREAD_START_ROUTINE)loadLibAddr,
							// pointer to the application-defined function of type LPTHREAD_START_ROUTINE to be executed by the thread
							allocatedRegionAddr,	// pointer to a variable to be passed to the thread function
							0,						// flags that control the creation of the thread (0 -  thread runs immediately after creation)
							NULL					// pointer to a variable that receives the thread identifier (NULL - the thread identifier is not returned
						);
						BOOL createRemoteThreadResult = NULL != hRemoteThread;

						if (createRemoteThreadResult) {
							// wait until remote thread terminates
							::WaitForSingleObject(
								hRemoteThread,	// handle to the object
								INFINITE		// time-out interval, in milliseconds (INFINITE - the function will return only when the object is signaled)
							);

							::CloseHandle(hProcess);
							//::VirtualFreeEx(hProcess, allocatedRegionAddr, dllName.length(), MEM_RELEASE);
						}
						else {
							std::cout << "Error creating remote thread." << std::endl;
						}

					}
					else {
						std::cout << "Error allocating of or writing to process memory." << std::endl;
					}
				}
				else {
					std::cout << "Error allocating of process memory." << std::endl;
				}
			}
			else {
				std::cout << "Error trying to access process." << std::endl;
			}

		}


		if (/*::FreeLibrary(hLibrary)*/TRUE) {
			std::cout << "Free successfully" << std::endl;
		}
		else {
			std::cout << "Cannot free DLL instance." << std::endl;
		}
	}
	else {
		std::cout << "Cannot find the DLL." << std::endl;
	}

	return 0;
}
