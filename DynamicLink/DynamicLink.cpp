﻿#include <iostream>
#include "windows.h"

#define LIBRARY_NAME "MyLib.dll"

typedef long(__cdecl* DLL_PROC_SUM)(int a, int b);

int main()
{
	std::cout << "Loading DLL dynamically." << std::endl;

	DLL_PROC_SUM sumProcAddr;
	HINSTANCE hLibrary;

	hLibrary = ::LoadLibrary(TEXT(LIBRARY_NAME));

	if (NULL != hLibrary) {
		sumProcAddr = (DLL_PROC_SUM)::GetProcAddress(hLibrary, "sum");

		if (NULL != sumProcAddr) {
			long res = sumProcAddr(10, 20);
			std::cout << "Sum = " << res << std::endl;
		}


		if (::FreeLibrary(hLibrary)) {
			std::cout << "Free successfully" << std::endl;
		}
		else {
			std::cout << "Cannot free DLL instance." << std::endl;
		}
	}
	else {
		std::cout << "Cannot find the DLL." << std::endl;
	}



	return 0;
}
