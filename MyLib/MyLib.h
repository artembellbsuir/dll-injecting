#pragma once

#ifdef MY_LIB_DLL
#define MY_LIB_API __declspec(dllexport)
#else
#define MY_LIB_API __declspec(dllimport)
#endif

extern "C" MY_LIB_API long sum(const int a, const int b);

extern "C" MY_LIB_API int replace(const char* find, const char* replace);