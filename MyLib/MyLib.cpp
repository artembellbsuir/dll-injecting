#include "pch.h"
#include "MyLib.h"
#include <iostream>

int replace(char* find, char* replace)
{
	size_t targetLen = strlen(find);
	size_t valueLen = strlen(replace);

	SYSTEM_INFO si;
	GetSystemInfo(&si);

	MEMORY_BASIC_INFORMATION info;

	auto minAddress = (LPSTR)si.lpMinimumApplicationAddress;
	auto maxAddress = (LPSTR)si.lpMaximumApplicationAddress;

	LPSTR currentAddress = minAddress;
	//auto baseAddress = si.lpMinimumApplicationAddress;

	while (currentAddress < maxAddress) {
		SIZE_T infoSize = sizeof(info);
		if (::VirtualQuery(
			currentAddress,
			&info,
			infoSize
		) == infoSize) {

			if (info.State == MEM_COMMIT && info.Protect == PAGE_READWRITE) {
				currentAddress = (LPSTR)info.BaseAddress;
				char* memory = (char*)malloc(info.RegionSize);
				SIZE_T bytesRead;

				if (::ReadProcessMemory(
					::GetCurrentProcess(),
					currentAddress,
					memory,
					info.RegionSize,
					&bytesRead
				)) {
					for (SIZE_T i = 0; i < bytesRead - targetLen; i++) {
						if (::strcmp(currentAddress + i, find) == 0) {
							::strcpy(currentAddress + i, replace);
						}
					}
				}

				free(memory);
			}
		}

		currentAddress += info.RegionSize;
	}

	return 0;
}



long sum(const int a, const int b) {
	long result = a + b;
	std::cout << "a = " << a << std::endl;
	std::cout << "b = " << b << std::endl;
	return result;
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH: {
		char findStr[] = "1337_1337";
		char replaceStr[] = "9999_7331";
		replace(findStr, replaceStr);
		break;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
